######################### IMPORTS #########################
import os, sys, string, re, csv, codecs, errno, shutil, msvcrt
from ctypes import windll, Structure, c_short, c_ushort, byref
from functools import reduce
from itertools import cycle
from Crypto.Cipher import AES
from Crypto import Random

######################## COLOR FUNCTIONS #######################

SHORT = c_short
WORD = c_ushort

class COORD(Structure):
  """struct in wincon.h."""
  _fields_ = [
    ("X", SHORT),
    ("Y", SHORT)]

class SMALL_RECT(Structure):
  """struct in wincon.h."""
  _fields_ = [
    ("Left", SHORT),
    ("Top", SHORT),
    ("Right", SHORT),
    ("Bottom", SHORT)]

class CONSOLE_SCREEN_BUFFER_INFO(Structure):
  """struct in wincon.h."""
  _fields_ = [
    ("dwSize", COORD),
    ("dwCursorPosition", COORD),
    ("wAttributes", WORD),
    ("srWindow", SMALL_RECT),
    ("dwMaximumWindowSize", COORD)]

# winbase.h
STD_INPUT_HANDLE = -10
STD_OUTPUT_HANDLE = -11
STD_ERROR_HANDLE = -12

# wincon.h
FOREGROUND_BLACK     = 0x0000
FOREGROUND_BLUE      = 0x0001
FOREGROUND_GREEN     = 0x0002
FOREGROUND_CYAN      = 0x0003
FOREGROUND_RED       = 0x0004
FOREGROUND_MAGENTA   = 0x0005
FOREGROUND_YELLOW    = 0x0006
FOREGROUND_GREY      = 0x0007
FOREGROUND_INTENSITY = 0x0008 # foreground color is intensified.

BACKGROUND_BLACK     = 0x0000
BACKGROUND_BLUE      = 0x0010
BACKGROUND_GREEN     = 0x0020
BACKGROUND_CYAN      = 0x0030
BACKGROUND_RED       = 0x0040
BACKGROUND_MAGENTA   = 0x0050
BACKGROUND_YELLOW    = 0x0060
BACKGROUND_GREY      = 0x0070
BACKGROUND_INTENSITY = 0x0080 # background color is intensified.

stdout_handle = windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE)
SetConsoleTextAttribute = windll.kernel32.SetConsoleTextAttribute
GetConsoleScreenBufferInfo = windll.kernel32.GetConsoleScreenBufferInfo

def get_text_attr():
  """Returns the character attributes (colors) of the console screen
  buffer."""
  csbi = CONSOLE_SCREEN_BUFFER_INFO()
  GetConsoleScreenBufferInfo(stdout_handle, byref(csbi))
  return csbi.wAttributes

def set_text_attr(color):
  """Sets the character attributes (colors) of the console screen
  buffer. Color is a combination of foreground and background color,
  foreground and background intensity."""
  SetConsoleTextAttribute(stdout_handle, color)

######################## FUNCTIONS #######################

# renames all subforders of dir, not including dir itself
def rename_all( root, items):
    for name in items:
        try:
            os.rename( os.path.join(root, name), os.path.join(root, name.lower()))
        except OSError:
            pass # can't rename it, so what

def lowerList(inputList):
    revisedList = []

    for i in inputList:
        revisedList.append(i.lower())

    return revisedList

def trimList(inputList):
    revisedList = []

    for i in inputList:
        if len(re.sub("[^\\w]", "", i)) > 0:
            revisedList.append(i)

    return revisedList

def analyzeStrings(stringList):
    stringList = lowerList(stringList)
    revisedStringList = []

    for i in stringList:
        if i.find("\\") >= 0:
            thePaths = repr(i).split("\\")
            for x in thePaths:
                if x.find(".") >= 0:
                    # file extension found, trim it off
                    thePos = x.find(".")
                    theStr = x[:thePos]
                    revisedStringList.append(re.sub("[^\\w]", "", theStr))
                else:
                    revisedStringList.append(re.sub("[^\\w]", "", x))
        elif i.find("_fnc_") >= 0 and not re.sub("[^\\w]", "", i) in ArmaFunctions:
            thePos = i.find("_fnc_")
            theEnd = thePos + 5
            thePrefix = i[:thePos]
            theSuffix = i[theEnd:]
            revisedStringList.append(re.sub("[^\\w]", "", thePrefix))
            revisedStringList.append(re.sub("[^\\w]", "", theSuffix))
        else:
            revisedStringList.append(i)

    revisedStringList = trimList(revisedStringList)
    return revisedStringList

def gatherLocals(inputString):
    revisedInputString = analyzeStrings(inputString)

    for i in revisedInputString:
        helperString = re.sub("[^\\w]", "", i)

        if (helperString.startswith('_') and
            not helperString in helperVariables and
            len(helperString) > 1 and
            helperString[1].isalpha() and
            not helperString == "_this" and
            not helperString == "_x" and
            not helperString == "_pos" and
            not helperString.lower() == "_foreachindex"):
            helperVariables.append(helperString)

def checkOtherVariables(value):
    found = False

    for var in otherVariables:
        varToCheck = var[0]
        if varToCheck.lower() == value.lower():
            found = True

    return found

def exportLocalVariables(array):
    header = ""
    data = ""

    # Gather Header
    for i in array:
        header = header + i[0] + "," + i[0] + ","
    header = header + "\n"

    # Gather Data
    for n in range(0, maxVariables):
        for i in array:
            numOfVariables = len(i[1])

            if n >= numOfVariables:
                data = data + ",,"
            else:
                curVars = i[1]
                curVarss = i[2]
                data = data + curVars[n] + "," + curVarss[n] + ","

        data = data + "\n"

    final = header + data

    return final

def exportOtherVariables(array):
    data = ""

    # Gather Data
    for i in array:
        data = data + i[0] + "," + i[1] + "," + "\n"

    return data


def encrypt(string, key):
    iv = Random.new().read(AES.block_size)
    cipher = AES.new(key, AES.MODE_CFB, iv)
    msg = iv + cipher.encrypt(string.encode(encoding='UTF-8'))

    encryptedMsgByte = codecs.encode(msg, 'hex')
    encryptedMsg = encryptedMsgByte.decode("utf-8")

    return encryptedMsg

def decrypt(string, key):
    iv = Random.new().read(AES.block_size)
    cipher = AES.new(key, AES.MODE_CFB, iv)

    decryptedMsgByte = cipher.decrypt(codecs.decode(string, 'hex'))[len(iv):]
    decryptedMsg = decryptedMsgByte.decode("utf-8")

    return decryptedMsg

def copy(src, dest):
    try:
        shutil.copytree(src, dest)
    except OSError as e:
        # If the error was caused because the source wasn't a directory
        if e.errno == errno.ENOTDIR:
            shutil.copy(src, dest)
        else:
            print('Directory not copied. Error: %s' % e)

def renameFolder(theRoot, theFolder, theFolderPath):
    for var in otherVariables:
        rawVar = var[0]
        encryptedVar = var[1]
        if rawVar == theFolder:
            if not theRoot[len(theRoot) - 1] == "\\":
                os.rename(theFolderPath, theRoot + "\\" + encryptedVar)
            else:
                os.rename(theFolderPath, theRoot + encryptedVar)

def renameFile(theRoot, theFile, theFileName, theFilePath, theFileExtension):
    for var in otherVariables:
        rawVar = var[0]
        encryptedVar = var[1]

        if rawVar == theFileName:
            if (theFile.find("fn_") < 0):
                os.rename(theFilePath, theRoot + "\\" + encryptedVar + theFileExtension)
            else:
                os.rename(theFilePath, theRoot + "\\fn_" + encryptedVar + theFileExtension)

def findWholeWord(w):
    return re.compile(r'\b({0})\b'.format(w), flags=re.IGNORECASE).search

def presentLine(fileName, line, raw, encrypted, start, end):
    # print and ask for confirmation
    default_colors = get_text_attr()
    default_bg = default_colors & 0x0070
    default_fg = default_colors & 0x0007

    os.system('cls')  # on windows
    print("File: ", fileName)
    print("Raw Variable: ", raw)
    print("Encrypted Variable: ", encrypted)
    print("Positions: [", start, ", ", end, "]", "\n")
    set_text_attr(default_colors)
    print(line[:start], end='')
    sys.stdout.flush() # Force writing first part of the line
    set_text_attr(FOREGROUND_RED | default_bg | FOREGROUND_INTENSITY)
    print(line[start:end], end='')
    sys.stdout.flush()
    set_text_attr(default_colors)
    print(line[end:])

    print("REPLACE THIS VARIABLE? [Y/N]:")
    print("OR PRESS [Z] TO END PROGRAM")
    decision = msvcrt.getch().decode("utf-8").lower()

    if decision == "z":
        os.system('cls')  # on windows
        sys.exit()

    return decision

##########################################################

os.system('cls')  # on windows

######################## VARIABLES #######################

localVariables = []
otherVariables = []
rawOtherVariables = []
maxVariables = 0
encryptionKey = b'AHBlack Saber XI'
trimEncryption = 20

globalHeaderTop = """/*
              _____  __  __          _    _  ____   _____ _______ _____
        /\   |  __ \|  \/  |   /\   | |  | |/ __ \ / ____|__   __/ ____|
       /  \  | |__) | \  / |  /  \  | |__| | |  | | (___    | | | (___
      / /\ \ |  _  /| |\/| | / /\ \ |  __  | |  | |\___ \   | |  \___ \

     / ____ \| | \ \| |  | |/ ____ \| |  | | |__| |____) |  | |  ____) |
    /_/    \_\_|  \_\_|  |_/_/    \_\_|  |_|\____/|_____/   |_| |_____/
    """

globalHeaderBottom = """
    Author: Garrett Bromley
    Website: www.ArmaHosts.com

    Using this code without ArmaHosts direct permission is strictly forbidden

*/

"""

##########################################################

##################### GET VARS/COMMANDS #####################
curDir = os.path.dirname(__file__)
functionsFile = "ArmaFunctions.txt"
commandsFile = "ArmaCommands.txt"
descriptionFile = "ArmaDescription.txt"
classnamesFile = "ArmaClassnames.txt"
defaultFiles = "ArmaDefaultFiles.txt"
functionsPath = os.path.join(curDir, functionsFile)
commandsPath = os.path.join(curDir, commandsFile)
descriptionPath = os.path.join(curDir, descriptionFile)
classnamesPath = os.path.join(curDir, classnamesFile)
defaultFilesPath = os.path.join(curDir, defaultFiles)

ArmaFunctions = []
ArmaCommands = []
ArmaDescription = []
ArmaClassnames = []
ArmaDefaultFiles = []

with open(functionsPath) as f:
    for line in f:
        ArmaFunctions.append(line[:-1].lower())

with open(commandsPath) as f:
    for line in f:
        ArmaCommands.append(line[:-1].lower())

with open(descriptionPath) as f:
    for line in f:
        ArmaDescription.append(line[:-1].lower())

with open(classnamesPath) as f:
    for line in f:
        ArmaClassnames.append(line[:-1].lower())

with open(defaultFilesPath) as f:
    for line in f:
        ArmaDefaultFiles.append(line[:-1].lower())

########################## MAIN ##########################
# get the directory we want to explore
print("Enter the Mission Directory")
print("Example: C:\\Users\\ArmaHosts\\Desktop\\myMission.Stratis\\")
path = input()
path = "C:\\Users\\garre\\OneDrive\\ArmaHosts\\Projects\\Obfuscator\\PRESET\\Input\\"

print("Enter the Mission Output Directory")
print("Example: C:\\Users\\ArmaHosts\\Desktop\\myObfuscatedMission.Stratis\\")
output = input()
output = "C:\\Users\\garre\\OneDrive\\ArmaHosts\\Projects\\Obfuscator\\PRESET\\OutputFinal\\"

####################### GET LOCALS #######################
for subdir, dirs, files in os.walk(path):
    for file in files:
        if file.endswith(".sqf") or file.endswith(".h") or file.endswith(".h") or file.endswith(".ext") or file.endswith(".hpp"):
            with open(os.path.join(subdir, file)) as f:
                #vlocal variable section
                helperArray = []
                helperVariables = []
                helperEncrypted = []

                for line in f:
                    theLine = line
                    theLine = theLine.replace(",", ", ")
                    theLine = theLine.replace(";", "; ")
                    gatherLocals(theLine.split())

                for i in helperVariables:
                    encryptedVar = "_AH" + encrypt(i[1:], encryptionKey)
                    helperEncrypted.append(encryptedVar[:trimEncryption])

                helperArray.append(file)
                helperArray.append(helperVariables)
                helperArray.append(helperEncrypted)

                if maxVariables < len(helperVariables) :
                    maxVariables = len(helperVariables)

                localVariables.append(helperArray)

####################### GET OTHERS #######################
# get the other variables, specified from the file
otherVariablesInput = "OtherVariables.txt"
otherVariablesPath = os.path.join(curDir, otherVariablesInput)

with open(otherVariablesPath) as f:
    for line in f:
        if (not line[:-1].lower() in rawOtherVariables and
            not line[:-1].lower() in ArmaFunctions and
            not line[:-1].lower() in ArmaCommands and
            not line[:-1].lower() in ArmaDescription and
            not line[:-1].lower() in ArmaClassnames):
            rawOtherVariables.append(line[:-1].lower())

# now go through the list and make an encrypted version of each variable
rawOtherVariables.sort(key=len, reverse=True)

for x in rawOtherVariables:
	curPos = rawOtherVariables.index(x)
	encryptedVar = "AH" + encrypt(x, encryptionKey)
	otherVariables.append([rawOtherVariables[curPos], encryptedVar[:trimEncryption]])

# print the variables to the output file
localVariableOutput = open("localVariableOutput.csv","w")
localVariableOutput.write(exportLocalVariables(localVariables))
localVariableOutput.close()

print("Local Variables Fetched, Encrypted and Exported...")

otherVariableOutput = open("otherVariableOutput.csv", "w")
otherVariableOutput.write(exportOtherVariables(otherVariables))
otherVariableOutput.close()

print("Other Variables Fetched, Encrypted and Exported...")

##################### REPLACE LOCALS #####################

copy(path, output)

for i in localVariables:
    fileName = i[0]
    rawVariables = i[1]
    encryptedVariables = i[2]
    fileData = ""

    if (len(rawVariables) > 0):
        for subdir, dirs, files in os.walk(output):
            for file in files:
                if file == fileName:
                    fileData = ""

                    with open(os.path.join(subdir, file)) as f:
                        for line in f:
                            theLine = line
                            lowerLine = theLine.lower()

                            for var in rawVariables:
                                indexPosition = rawVariables.index(var)
                                rawVar = rawVariables[indexPosition]
                                encVar = encryptedVariables[indexPosition]

                                while (findWholeWord(rawVar)(lowerLine) != None):
                                    thePos = findWholeWord(rawVar)(lowerLine).span()[0]
                                    theEnd = findWholeWord(rawVar)(lowerLine).span()[1]
                                    theLine = theLine[:thePos] + encVar + theLine[theEnd:]
                                    lowerLine = theLine.lower()

                            fileData = fileData + theLine

                    with open(os.path.join(subdir, file), 'w') as f:
                        f.write(fileData)

print("Local Variables Replaced...")

################# DELETE COMMENTS/CONDENSE #################

# Go through the files and search for // and delete everything in the rest of the line if found
for subdir, dirs, files in os.walk(output):
    for file in files:
        if file.endswith(".sqf") or file.endswith(".h") or file.endswith(".h") or file.endswith(".ext") or file.endswith(".hpp"):
            newFileData = ""

            with open(os.path.join(subdir, file)) as f:
                for line in f:
                    commentPos = line.find("//") - 1

                    # not found
                    if commentPos == -2:
                        newFileData = newFileData + line

                    # in the line
                    if commentPos >= 0:
                        newFileData = newFileData + line[:commentPos]

            with open(os.path.join(subdir, file), 'w') as f:
                f.write(newFileData)

            newFileData = ""

            with open(os.path.join(subdir, file)) as f:
                inBlock = 0
                for line in f:
                    if inBlock == 0:
                        commentPos = line.find("/*") - 1
                        # not found
                        if commentPos == -2:
                            newFileData = newFileData + line
                        else:
                            inBlock = 1
                        if commentPos >= 0:
                            newFileData = newFileData + line[:commentPos]

                    if inBlock == 1:
                        commentPos = line.find("*/") - 1
                        # found
                        if commentPos > -2:
                            inBlock = 0

            with open(os.path.join(subdir, file), 'w') as f:
                f.write(newFileData)

##################### REPLACE OTHERS #####################

for subdir, dirs, files in os.walk(output):
    for file in files:
        if file.endswith(".sqf") or file.endswith(".h") or file.endswith(".ext") or file.endswith(".hpp") or file.endswith(".sqm"):
            fileData = ""

            with open(os.path.join(subdir, file)) as f:
                if file.endswith(".sqm"):
                    inEntities = 0
                    for line in f:
                        if line.find("class Entities") >= 0:
                            inEntities = 1
                        if line.find("class Connections") >= 0:
                            inEntities = 0

                        if inEntities == 1 and (line.find('name="') >= 0 or line.find('init="') >= 0 or line.find('value="') >= 0 or line.find('condition="') >= 0 or line.find('onActivation="') >= 0 or line.find('onDeactivation="') >= 0):
                            theLine = line
                            theLine = theLine.replace("_fnc_", " _fnc_ ")
                            theLine = theLine.replace("fn_", " fn_ ")
                            lowerLine = theLine.lower()

                            for var in otherVariables:
                                rawVar = var[0]
                                encVar = var[1]
                                cursor = 0

                                while (findWholeWord(rawVar)(lowerLine[cursor:]) != None):
                                    thePos = findWholeWord(rawVar)(lowerLine[cursor:]).span()[0] + cursor
                                    theEnd = findWholeWord(rawVar)(lowerLine[cursor:]).span()[1] + cursor
                                    theDecision = presentLine(file, theLine, rawVar, encVar, thePos, theEnd)
                                    if theDecision == "y":
                                        theLine = theLine[:thePos] + encVar + theLine[theEnd:]
                                    lenDiff = len(encVar) - len(rawVar)
                                    cursor = theEnd + lenDiff
                                    lowerLine = theLine.lower()

                            theLine = theLine.replace(" _fnc_ ", "_fnc_")
                            theLine = theLine.replace(" fn_ ", "fn_")
                            fileData = fileData + theLine
                        else:
                            fileData = fileData + line
                else:
                    for line in f:
                        theLine = line
                        theLine = theLine.replace("_fnc_", " _fnc_ ")
                        theLine = theLine.replace("fn_", " fn_ ")
                        lowerLine = theLine.lower()

                        for var in otherVariables:
                            rawVar = var[0]
                            encVar = var[1]
                            cursor = 0

                            while (findWholeWord(rawVar)(lowerLine[cursor:]) != None):
                                thePos = findWholeWord(rawVar)(lowerLine[cursor:]).span()[0] + cursor
                                theEnd = findWholeWord(rawVar)(lowerLine[cursor:]).span()[1] + cursor
                                theDecision = presentLine(file, theLine, rawVar, encVar, thePos, theEnd)
                                if theDecision == "y":
                                    theLine = theLine[:thePos] + encVar + theLine[theEnd:]
                                lenDiff = len(encVar) - len(rawVar)
                                cursor = theEnd + lenDiff
                                lowerLine = theLine.lower()

                        theLine = theLine.replace(" _fnc_ ", "_fnc_")
                        theLine = theLine.replace(" fn_ ", "fn_")
                        fileData = fileData + theLine

            with open(os.path.join(subdir, file), 'w') as f:
                f.write(fileData)


print("Other Variables Replaced...")

# Delete Whitespace and Condense

for subdir, dirs, files in os.walk(output):
    for file in files:
        if file.endswith(".sqf") or file.endswith(".h") or file.endswith(".h") or file.endswith(".ext") or file.endswith(".hpp"):
            newFileData = ""

            with open(os.path.join(subdir, file)) as f:
                for line in f:
                    firstChar = len(line) - len(line.lstrip())
                    newFileData = newFileData + line[firstChar:]

            with open(os.path.join(subdir, file), 'w') as f:
                f.write(newFileData)

            newFileData = ""

            with open(os.path.join(subdir, file)) as f:
                for line in f:
                    if line.find("#") == 0:
                        newFileData = newFileData + "\n" + line
                    else:
                        newFileData = newFileData + line.rstrip("\n") + " "

            with open(os.path.join(subdir, file), 'w') as f:
                encFileName = ""
                for var in otherVariables:
                    rawVar = var[0]
                    encVar = var[1]
                    if file == rawVar:
                        encFileName = encVar
                f.write(globalHeaderTop + encFileName + globalHeaderBottom + newFileData)

print("Line Comments Deleted...")
print("Block Comments Deleted...")
print("Whitespace Trimmed...")
print("Lines Condensed...")
print("Headers Added...")

for root, dirs, files in os.walk(output, topdown=False):
    rename_all( root, dirs )
    rename_all( root, files)

################# CHANGE FILE/FOLDER NAMES #################
for root, subFolders, files in os.walk(output, topdown=False):
    for folder in subFolders:
        renameFolder(root, folder, os.path.join(root, folder))

for root, subFolders, files in os.walk(output):
    for file in files:
        if file.find("fn_") < 0:
            fileName = file[:file.find(".")]
        else:
            fileName = file[3:file.find(".")]

        fileExtension = file[file.find("."):]
        if not fileName in ArmaDefaultFiles:
            renameFile(root, file, fileName, os.path.join(root, file), fileExtension)

print("Folders and Files Encrypted...")
